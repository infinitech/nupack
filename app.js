var fs = require('fs');
var Job = require('./src/Job.js').Job;
var JobReader = require('./src/JobReader.js').JobReader;
var CostCalculator = require('./src/CostCalculator.js').CostCalculator;
var OutputWriter = require('./src/OutputWriter.js').OutputWriter;
var util = require('./src/util.js');

if (process.argv.length !== 3) {
    console.error('Exactly one argument required');
    process.exit(1);
}

var input = process.argv[2];

fs.readFile(input, 'utf-8', function (err, text) {
    if (err) throw err;
    var jobreader = new JobReader(text);
    var data = jobreader.getData();

    var jobCollection = [];

    if(!data.length) console.log('The input file is not valid');
    else {
        data.forEach(function(item){
            job = new Job(
                item['cost'],
                item['people'],
                item['materials']
            );
            jobCollection.push(job);
        });

        var costcalculator = new CostCalculator(jobCollection);
        var outputwriter = new OutputWriter(costcalculator.getCosts());
        console.log(outputwriter.generateOutput());
    }
});
