describe("JobReader", function() {

   var jobreader;

   describe("Invalid Input", function() {
      it("Should consider empty data to be invalid", function() {
         jobreader = new JobReader("");
         expect(jobreader.isValid()).toBeFalsy();

         jobreader = new JobReader("   ");
         expect(jobreader.isValid()).toBeFalsy();
      });
      
      it("Should consider input without at least one block of jobs invalid", function() {
         jobreader = new JobReader("blah");
         expect(jobreader.isValid()).toBeFalsy();
      });

       it("Should consider input with an incorrectly specified cost to be invalid", function() {
           jobreader = new JobReader(
               "Input 1:\n" +
                   "--------\n" +
                   "asdf\n" +
                   "3 people\n" +
                   "food"
           );
           expect(jobreader.isValid()).toBeFalsy();
       });

       it("Should consider input with an incorrectly specified number of people to be invalid", function() {
           jobreader = new JobReader(
               "Input 1:\n" +
                   "--------\n" +
                   "$1000.00\n" +
                   "blah\n" +
                   "food"
           );
           expect(jobreader.isValid()).toBeFalsy();
       });

       it("Should consider input with zero people to be invalid", function() {
           jobreader = new JobReader(
               "Input 1:\n" +
                   "--------\n" +
                   "$1000.00\n" +
                   "0 people\n" +
                   "food"
           );
           expect(jobreader.isValid()).toBeFalsy();
       });

       it("Should consider input with no materials to be invalid", function() {
           jobreader = new JobReader(
               "Input 1:\n" +
                   "--------\n" +
                   "$1000.00\n" +
                   "2 people\n" +
                   ""
           );
           expect(jobreader.isValid()).toBeFalsy();
       });
   });

   describe("Valid Input", function() {
      it("Should consider input with one block valid", function() {
         jobreader = new JobReader(
            "Input 1:\n" + 
            "--------\n" + 
            "$1299.99\n" +
            "3 people\n" + 
            "food"
         );
         expect(jobreader.isValid()).toBeTruthy();
      });
      
      it("Should consider input with multiple blocks valid", function() {
         jobreader = new JobReader(
            "Input 1:\n" + 
            "--------\n" + 
            "$1299.99\n" +
            "3 people\n" + 
            "food\n" + 
            "\n" + 
            "Input 2:\n" + 
            "--------\n" + 
            "$5432.00\n" +
            "1 person\n" + 
            "drugs\n" + 
            "\n" + 
            "Input 3:\n" + 
            "--------\n" + 
            "$12456.95\n" +
            "4 people\n" + 
            "books"
         );
         expect(jobreader.isValid()).toBeTruthy();
      }); 
   });

   describe("Should parse data correctly", function() {

         beforeEach(function() {
            jobreader1 = new JobReader(
               "Input 1:\n" + 
               "--------\n" + 
               "$1299.99\n" +
               "3 people\n" + 
               "food"
            );
            jobreader2 = new JobReader(
               "Input 2:\n" + 
               "--------\n" + 
               "$5432.00\n" +
               "1 person\n" + 
               "drugs"
            );

            data1 = jobreader1.getData()[0];
            data2 = jobreader2.getData()[0];
         });

         it("Should parse the cost correctly", function() {
            expect(data1.cost).toEqual('129999');
            expect(data2.cost).toEqual('543200');
         });

         it("Should parse the number of people correctly", function() {
            expect(data1.people).toEqual('3');
            expect(data2.people).toEqual('1');
         });

         it("Should parse the materials correctly", function() {
            expect(data1.materials).toEqual('food');
            expect(data2.materials).toEqual('drugs');
         });

      });


});
