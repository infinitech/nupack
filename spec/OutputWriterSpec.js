describe("OutputWriter", function() {

    var outputwriter;

    describe("Handling various input", function() {

        it("Should not print anything if no data is passed in", function() {
            outputwriter = new OutputWriter([]);
            expect(outputwriter.generateOutput()).toEqual('');
        });

        it("Should print a single line with no newline at the end", function() {
            outputwriter = new OutputWriter([1000]);
            expect(outputwriter.generateOutput()).toEqual('Output 1: $1000.00');
        });

    });

});
