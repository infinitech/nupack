describe("CostCalculator", function() {

    var costcalculator;

    describe("Calculate correct costs", function() {

        it("Should charge flat markup (5%) and 1.2% on a job with 1 person", function() {
            job = new Job('100000','1','foo');

            costcalculator = new CostCalculator([job]);
            var costs = costcalculator.getCosts();
            expect(costs.length).toEqual(1);
            expect(costs[0]).toEqual(formatCost((100000 * 1.05 * (1*1.12))/100));
        });

        it("Should charge flat markup (5%) and 1.2% for each person in the job", function() {
            job = new Job('100000','3','foo');

            costcalculator = new CostCalculator([job]);
            var costs = costcalculator.getCosts();
            expect(costs.length).toEqual(1);
            expect(costs[0]).toEqual(formatCost((100000 * 1.05 * (3*1.12))/100));
        });

        it("Should calculate the costs correctly when there are multiple jobs", function() {
            job1 = new Job('100000','2','foo');
            job2 = new Job('200000','3','foo');

            costcalculator = new CostCalculator([job1, job2]);
            var costs = costcalculator.getCosts();
            expect(costs.length).toEqual(2);
            expect(costs[0]).toEqual(formatCost((100000 * 1.05 * (2*1.12))/100));
            expect(costs[1]).toEqual(formatCost((200000 * 1.05 * (3*1.12))/100));
        });

        it("Should calculate the pharmaceuticals markup correctly", function() {
            job1 = new Job('100000','1','pharmaceuticals');

            costcalculator = new CostCalculator([job1]);
            var costs = costcalculator.getCosts();
            expect(costs[0]).toEqual(formatCost((100000 * 1.05 * (1*1.12) * 1.075)/100));
        });

        it("Should calculate the food markup correctly", function() {
            job1 = new Job('100000','1','food');

            costcalculator = new CostCalculator([job1]);
            var costs = costcalculator.getCosts();
            expect(costs[0]).toEqual(formatCost((100000 * 1.05 * (1*1.12) * 1.13)/100));
        });

        it("Should calculate the electronics markup correctly", function() {
            job1 = new Job('100000','1','electronics');

            costcalculator = new CostCalculator([job1]);
            var costs = costcalculator.getCosts();
            expect(costs[0]).toEqual(formatCost((100000 * 1.05 * (1*1.12) * 1.02)/100));
        });

        function formatCost(cost) {
            return parseFloat(cost).toFixed(2)
        }

    });
});
