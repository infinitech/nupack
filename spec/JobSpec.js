describe("Job", function() {

   var job;

   describe("Initialising a Job", function() {

      it("Should initialise a job correctly for 1 person", function() {
         job = new Job('100000','1','food');
         expect(job.people).toEqual('1 person');
      });

       it("Should initialise a job correctly for multiple people", function() {
           job = new Job('100000','3','food');
           expect(job.people).toEqual('3 people');
       });

       it("Should initialise a job correctly for zero dollars", function() {
           job = new Job('0','1','food');
           expect(job.cost).toEqual('$0.00');
       });

       it("Should initialise a job correctly for the given materials", function() {
           job = new Job('100000','1','food');
           expect(job.materials).toEqual('food');
       });

   });

});
