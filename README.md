Overview
========

This is a coding exercise that uses BDD to develop a solution to the following
problem.

Pricing Problem
===============

NuPack is responsible for taking existing products and repackaging them for
sale at electronic stores like Future Shop and Best Buy. Companies will phone
up NuPack, explain the process and NuPack needs to quickly give them an
estimate of how much it will cost. Different markups to the job:

* Without exception, there is a flat markup on all jobs of 5%
* For each person that needs to work on the job, there is a markup of 1.2%

Markups are also added depending on the types of materials involved:

* If pharmaceuticals are involved, there is an immediate 7.5% markup
* For food, there is a 13% markup
* Electronics require a 2% markup
* Everything else, there is no markup

Another system calculates the base price depending on how many products need 
to be repackaged. As such, the markup calculator should accept the initial base 
price along with the different categories of markups and calculate a final cost 
for a project.

The flat markup is calculated first and then all other markups are calculated on top of the base price plus flat markup.

For Example
===========

Input 1:
--------
$1299.99  
3 people  
food

Input 2:
--------
$5432.00  
1 person  
drugs

Input 3:
--------
$12456.95  
4 people  
books


Output 1: $1591.58  
Output 2: $6199.81  
Output 3: $13707.64

Requirements
============

- node.js (tested on v0.8.21)
- jasmine (tested on v2.0.0)

Usage
=====

There is a sample input.txt file in this directory.

To execute the program with the included input file, run the following.

	node ./app.js input.txt

Note: The input file should NOT have a trailing blank line. Othewise it will be considered invalid input