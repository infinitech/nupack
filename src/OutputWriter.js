exports.OutputWriter = OutputWriter;

function OutputWriter(data)
{
    this.data = data;

    this.generateOutput = function() {
        var outputarray = [];
        for(var i = 1; i <= this.data.length; i++) {
            outputarray.push("Output " + i + ": $" + parseFloat(this.data[i-1]).toFixed(2));
        }
        return outputarray.join('\n');
    };
}
