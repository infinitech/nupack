exports.CostCalculator = CostCalculator;

function CostCalculator(jobs) {
    var that = this;
    this.data = jobs;
    this.results = [];

    this.getCosts = function() {
        this.data.forEach(function(job) {
            var cost = that.chargeFlatRate(job.getRawCost());
            cost = that.chargePersonRate(cost, job.getRawPeople());
            cost = that.chargeMaterialMarkup(cost, job.materials);
            that.results.push(parseFloat(cost/100).toFixed(2));
        });
        return this.results;
    };

    this.chargeFlatRate = function(value) {
        return value * 1.05;
    };

    this.chargePersonRate = function(value, number_of_people) {
        return value * 1.12 * number_of_people;
    };

    this.chargeMaterialMarkup = function(value, materials) {

        var rates = {
            "pharmaceuticals": 1.075,
            "food": 1.13,
            "electronics": 1.02
        }

        return rates[materials] !== undefined ? rates[materials] * value : value;
    };

}
