exports.Job = Job;

function Job(cost, people, materials)
{
    var cost = cost;
    var people = people;
    this.materials = materials;

    this.__defineGetter__("cost", function(){
        return '$' + parseFloat(cost/100).toFixed(2);
    });

    this.__defineSetter__("cost", function(newCost){
        cost = newCost;
    });

    this.__defineGetter__("people", function(){
        return people + ' ' + (parseInt(people) > 1 ? 'people' : 'person');
    });

    this.__defineSetter__("people", function(newPeople){
        people = newPeople;
    });

    this.getRawCost = function() {
        return cost;
    };

    this.getRawPeople = function() {
        return people;
    };


}
