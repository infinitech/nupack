exports.JobReader = JobReader;

function JobReader(data) {
   var that = this;
  this.data = data.split("\n").chunk(6);
  this.isValid = function() {
      var chunkLength = this.data.length;
      var lastPiece = this.data[chunkLength-1];
      if(lastPiece.length !== 5) return false;

      for(var i = 0; i < chunkLength; i++) {
          if(!that.validate(that.data[i])) return false;
      }

      return true;
  };

  this.getData = function() {
      if(this.isValid()) {
         var returArray = [];
   
         this.data.forEach(function(record) {
            returArray.push({
               'cost': that.getCost(record[2]),
               'people': that.getPeople(record[3]),
               'materials': that.getMaterials(record[4])
            });
         });

         return returArray;
      }
      return [];
  };

  this.getCost = function(data) {
      return data.replace(/\$/g, '').replace(/\./g, '');
  };

  this.getPeople = function(data) {
      return data.replace(/ people/g, '').replace(/ person/g, '');
  }

  this.getMaterials = function(data) {
      return data; 
  };

  this.validate = function(record) {
        return validateCost(that.getCost(record[2])) && validatePeople(that.getPeople(record[3])) && validateMaterials(that.getMaterials(record[4]));
  };

  function validateCost(cost) {
      return isInt(cost);
  }

    function validatePeople(people) {
        return isInt(people) && people > 0;
    }

    function validateMaterials(materials) {
        return materials.length > 0;
    }

    function isInt(value) {
        return value % 1 == 0;
    }

}
