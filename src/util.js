Array.prototype.chunk = function(size) {
    var newArray = [];
    for (var i=0; i<this.length; i+=size) {
        newArray.push(this.slice(i,i+size));
    }
    return newArray;
}
